﻿using Checkers.Enums;
using Checkers.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Checkers.Models
{
	public class PlayerTurn : BaseNotification
	{
		private PieceColor color;
		private string? image;

		public PlayerTurn(PieceColor color)
		{
			this.color = color;
			loadImages();
		}

        public PlayerTurn()
        {
            loadImages();
        }

        public void loadImages()
		{
			if (color == PieceColor.Red)
			{
				image = Utility.redPiece;
				return;
			}
			image = Utility.whitePiece;
		}

		public PieceColor PlayerColor
		{
			get { return color; }
			set
			{
				color = value;
				NotifyPropertyChanged(nameof(PlayerColor));
			}
		}

		public string? TurnImage
		{
			get
			{
				return image;
			}
			set
			{
				image = value;
				NotifyPropertyChanged(nameof(TurnImage));
			}
		}
	}
}
