﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Checkers.Models
{
	public class Winner(int redWins, int whiteWins) : BaseNotification
	{
		private int redWins = redWins;
		private int whiteWins = whiteWins;

		public int RedWins
		{
			get
			{
				return redWins;
			}
			set
			{
				redWins = value;
				NotifyPropertyChanged(nameof(RedWins));
			}
		}

		public int WhiteWins
		{
			get
			{
				return whiteWins;
			}
			set
			{
				whiteWins = value;
				NotifyPropertyChanged(nameof(WhiteWins));
			}
		}
	}
}
