﻿using Checkers.Models;

namespace Checkers.Services
{
    public class GameState
    {
        public GameSquare CurrentSquare { get; set; }
        public Dictionary<GameSquare, GameSquare> CurrentNeighbours { get; set; } = new Dictionary<GameSquare, GameSquare>();
        public bool ExtraMove { get; set; }
        public bool ExtraPath { get; set; }
        public PlayerTurn Turn { get; set; } = new PlayerTurn();
    }
}
