﻿using Checkers.Enums;
using Checkers.Models;
using System.Collections.ObjectModel;
using System.Windows;

namespace Checkers.Services;

public partial class GameLogicBase
{
    protected ObservableCollection<ObservableCollection<GameSquare>> board;
    protected PlayerTurn playerTurn;
    protected Winner winner;
    protected GameController gameController;

    public GameLogicBase(ObservableCollection<ObservableCollection<GameSquare>> board, PlayerTurn playerTurn, Winner winner)
    {
        this.board = board;
        this.playerTurn = playerTurn;
        this.winner = winner;
        this.winner.RedWins = Utility.getScore().RedWins;
        this.winner.WhiteWins = Utility.getScore().WhiteWins;
        gameController = new GameController(board);
    }

    protected void SwitchTurns(GameSquare square)
    {
        if (square.Piece.Color == PieceColor.Red)
        {
            Utility.Turn.PlayerColor = PieceColor.White;
            Utility.Turn.TurnImage = Utility.whitePiece;
            playerTurn.PlayerColor = PieceColor.White;
            playerTurn.TurnImage = Utility.whitePiece;
        }
        else
        {
            Utility.Turn.PlayerColor = PieceColor.Red;
            Utility.Turn.TurnImage = Utility.redPiece;
            playerTurn.PlayerColor = PieceColor.Red;
            playerTurn.TurnImage = Utility.redPiece;
        }
    }

    protected void FindNeighbours(GameSquare square)
    {
        var neighboursToCheck = new HashSet<Tuple<int, int>>();

        Utility.initializeNeighboursToBeChecked(square, neighboursToCheck);

        foreach (Tuple<int, int> neighbour in neighboursToCheck)
        {
            if (Utility.isInBounds(square.Row + neighbour.Item1, square.Column + neighbour.Item2))
            {
                if (board[square.Row + neighbour.Item1][square.Column + neighbour.Item2].Piece == null)
                {
                    if (!Utility.ExtraMove)
                    {
                        Utility.CurrentNeighbours.Add(board[square.Row + neighbour.Item1][square.Column + neighbour.Item2], null);
                    }
                }
                else if (Utility.isInBounds(square.Row + neighbour.Item1 * 2, square.Column + neighbour.Item2 * 2) &&
                    board[square.Row + neighbour.Item1][square.Column + neighbour.Item2].Piece.Color != square.Piece.Color &&
                    board[square.Row + neighbour.Item1 * 2][square.Column + neighbour.Item2 * 2].Piece == null)
                {
                    Utility.CurrentNeighbours.Add(board[square.Row + neighbour.Item1 * 2][square.Column + neighbour.Item2 * 2], board[square.Row + neighbour.Item1][square.Column + neighbour.Item2]);
                    Utility.ExtraPath = true;
                }
            }
        }
    }

    protected void DisplayRegularMoves(GameSquare square)
    {
        if (Utility.CurrentSquare != square)
        {
            if (Utility.CurrentSquare != null)
            {
                board[Utility.CurrentSquare.Row][Utility.CurrentSquare.Column].Texture = Utility.redSquare;

                foreach (GameSquare selectedSquare in Utility.CurrentNeighbours.Keys)
                {
                    selectedSquare.LegalSquareSymbol = null;
                }
                Utility.CurrentNeighbours.Clear();
            }

            FindNeighbours(square);

            if (Utility.ExtraMove && !Utility.ExtraPath)
            {
                Utility.ExtraMove = false;
                SwitchTurns(square);
            }
            else
            {
                foreach (GameSquare neighbour in Utility.CurrentNeighbours.Keys)
                {
                    board[neighbour.Row][neighbour.Column].LegalSquareSymbol = Utility.hintSquare;
                }

                Utility.CurrentSquare = square;
                Utility.ExtraPath = false;
            }
        }
        else
        {
            board[square.Row][square.Column].Texture = Utility.redSquare;

            foreach (GameSquare selectedSquare in Utility.CurrentNeighbours.Keys)
            {
                selectedSquare.LegalSquareSymbol = null;
            }
            Utility.CurrentNeighbours.Clear();
            Utility.CurrentSquare = null;
        }
    }

    protected void GameOver()
    {
        Winner aux = Utility.getScore();
        if (Utility.CollectedRedPieces == 12)
        {
            Utility.writeScore(aux.RedWins, ++aux.WhiteWins);
        }
        if (Utility.CollectedWhitePieces == 12)
        {
            Utility.writeScore(++aux.RedWins, aux.WhiteWins);
        }
        winner.RedWins = aux.RedWins;
        winner.WhiteWins = aux.WhiteWins;
        Utility.CollectedRedPieces = 0;
        Utility.CollectedWhitePieces = 0;
        MessageBox.Show("You won! You are the best :3");
        gameController.ResetGame();
    }
}
