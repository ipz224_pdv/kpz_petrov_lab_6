﻿using Checkers.Enums;
using Checkers.Models;
using System.Collections.ObjectModel;
using System.IO;


namespace Checkers.Services
{
    public class Utility
    {
        #region constValues
        //const image paths
        public const string redSquare = "/Checkers;component/Resources/squareRed.png";
        public const string whiteSquare = "/Checkers;component/Resources/squareWhite.png";
        public const string redPiece = "/Checkers;component/Resources/pieceRed.png";
        public const string whitePiece = "/Checkers;component/Resources/pieceWhite.png";
        public const string hintSquare = "/Checkers;component/Resources/squareHint.png";
        public const string redKingPiece = "/Checkers;component/Resources/pieceRedKing.png";
        public const string whiteKingPiece = "/Checkers;component/Resources/pieceWhiteKing.png";
        public const string SQUARE_HIGHLIGHT = "NULL";
        //const values for serialization
        public const char NO_PIECE = 'N';
        public const char WHITE_PIECE = 'W';
        public const char RED_PIECE = 'R';
        public const char RED_KING = 'K';
        public const char WHITE_KING = 'L';
        public const char WHITE_TURN = '2';
        public const char RED_TURN = '1';
        public const char HAD_COMBO = 'H';
        public const char EXTRA_PATH = 'E';
        //board constants
        public const int boardSize = 8;
        #endregion
        #region staticValues
        public static GameSquare? CurrentSquare { get; set; }
        private static Dictionary<GameSquare, GameSquare> currentNeighbours = new Dictionary<GameSquare, GameSquare>();
        private static PlayerTurn turn = new PlayerTurn(PieceColor.Red);
        private static bool extraMove = false;
        private static bool extraPath = false;
        private static int collectedRedPieces = 0;
        private static int collectedWhitePieces = 0;
        #endregion

        public static Dictionary<GameSquare, GameSquare> CurrentNeighbours
        {
            get
            {
                return currentNeighbours;
            }
            set
            {
                currentNeighbours = value;
            }
        }
        public static PlayerTurn Turn
        {
            get
            {
                return turn;
            }
            set
            {
                turn = value;
            }
        }
        public static bool ExtraMove
        {
            get
            {
                return extraMove;
            }
            set
            {
                extraMove = value;
            }
        }
        public static bool ExtraPath
        {
            get
            {
                return extraPath;
            }
            set
            {
                extraPath = value;
            }
        }

        public static int CollectedWhitePieces
        {
            get { return collectedWhitePieces; }
            set { collectedWhitePieces = value; }
        }

        public static int CollectedRedPieces
        {
            get { return collectedRedPieces; }
            set { collectedRedPieces = value; }
        }


        #region UtilityMethods
        public static ObservableCollection<ObservableCollection<GameSquare>> initBoard()
        {
            ObservableCollection<ObservableCollection<GameSquare>> board = [];
            const int boardSize = 8;

            for (int row = 0; row < boardSize; ++row)
            {
                board.Add([]);
                for (int column = 0; column < boardSize; ++column)
                {
                    if ((row + column) % 2 == 0)
                    {
                        board[row].Add(new GameSquare(row, column, SquareShade.Light, null));
                    }
                    else if (row < 3)
                    {
                        board[row].Add(new GameSquare(row, column, SquareShade.Dark, new GamePiece(PieceColor.White)));
                    }
                    else if (row > 4)
                    {
                        board[row].Add(new GameSquare(row, column, SquareShade.Dark, new GamePiece(PieceColor.Red)));
                    }
                    else
                    {
                        board[row].Add(new GameSquare(row, column, SquareShade.Dark, null));
                    }
                }
            }

            return board;
        }

        public static bool isInBounds(int row, int column)
        {
            return row >= 0 && column >= 0 && row < boardSize && column < boardSize;
        }

        public static void initializeNeighboursToBeChecked(GameSquare square, HashSet<Tuple<int, int>> neighboursToCheck)
        {
            if (square.Piece.Type == PieceType.King)
            {
                neighboursToCheck.Add(new Tuple<int, int>(-1, -1));
                neighboursToCheck.Add(new Tuple<int, int>(-1, 1));
                neighboursToCheck.Add(new Tuple<int, int>(1, -1));
                neighboursToCheck.Add(new Tuple<int, int>(1, 1));
            }
            else if (square.Piece.Color == PieceColor.Red)
            {
                neighboursToCheck.Add(new Tuple<int, int>(-1, -1));
                neighboursToCheck.Add(new Tuple<int, int>(-1, 1));
            }
            else
            {
                neighboursToCheck.Add(new Tuple<int, int>(1, -1));
                neighboursToCheck.Add(new Tuple<int, int>(1, 1));
            }
        }
        #endregion



        #region textFileHandling
        public static void writeScore(int r, int w)
        {
            string projectDirectory = AppDomain.CurrentDomain.BaseDirectory;
            string relativePath = Path.Combine(projectDirectory, "Resources", "winnerText.txt");
            using var writer = new StreamWriter(relativePath);
            writer.WriteLine(r + "," + w);
        }

        public static Winner getScore()
        {
            Winner aux = new Winner(0, 0);
            string projectDirectory = AppDomain.CurrentDomain.BaseDirectory;
            string relativePath = Path.Combine(projectDirectory, "Resources", "winnerText.txt");
            using (var reader = new StreamReader(relativePath))
            {
                string line = reader.ReadLine();
                var splitted = line.Split(",");
                aux.RedWins = int.Parse(splitted[0]);
                aux.WhiteWins = int.Parse(splitted[1]);
            }
            return aux;
        }
        #endregion
    }
}
