﻿using Checkers.Enums;
using Checkers.Models;
using System.Collections.ObjectModel;


namespace Checkers.Services
{
    public class GameResetter
    {
        private static GameState _gameState;
        public static void ResetGame(ObservableCollection<ObservableCollection<GameSquare>> squares)
        {
            _gameState = new GameState();
            foreach (var square in _gameState.CurrentNeighbours.Keys)
            {
                square.LegalSquareSymbol = null;
            }

            if (_gameState.CurrentSquare != null)
            {
                _gameState.CurrentSquare.Texture = GameConstants.redSquare;
            }

            _gameState.CurrentNeighbours.Clear();
            _gameState.CurrentSquare = null;
            _gameState.ExtraMove = false;
            _gameState.ExtraPath = false;
            GameConstants.CollectedWhitePieces = 0;
            GameConstants.CollectedRedPieces = 0;
            _gameState.Turn.PlayerColor = PieceColor.Red;

            ResetGameBoard(squares);
        }

        public static void ResetGameBoard(ObservableCollection<ObservableCollection<GameSquare>> squares)
        {
            for (int index1 = 0; index1 < GameConstants.BOARD_SIZE; index1++)
            {
                for (int index2 = 0; index2 < GameConstants.BOARD_SIZE; index2++)
                {
                    if ((index1 + index2) % 2 == 0)
                    {
                        squares[index1][index2].Piece = null;
                    }
                    else if (index1 < 3)
                    {
                        squares[index1][index2].Piece = new GamePiece(PieceColor.White)
                        {
                            Square = squares[index1][index2]
                        };
                        //pieces
                    }
                    else if (index1 > 4)
                    {
                        squares[index1][index2].Piece = new GamePiece(PieceColor.Red)
                        {
                            Square = squares[index1][index2]
                        };
                        //pieces
                    }
                    else
                    {
                        squares[index1][index2].Piece = null;
                    }
                }
            }
        }
    }
}
