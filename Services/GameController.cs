﻿using Checkers.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Checkers.Services
{
    public class GameController
    {
        private ObservableCollection<ObservableCollection<GameSquare>> squares;

        public GameController(ObservableCollection<ObservableCollection<GameSquare>> squares)
        {
            this.squares = squares;
        }

        public void LoadGameFromFile()
        {
            GameLoader.LoadGame(squares);
        }

        public void SaveGameToFile()
        {
            GameSaver.SaveGame(squares);
        }

        public void ResetGame()
        {
            GameResetter.ResetGame(squares);
        }
    }
}
