# MVVM Checkers Game 

## Description

### Programming principles

#### SRP

Single responsibility principle is followed in the project, classes are only responsible for things they are meant to do. Classes in models do their job, for example, the [Winner](./Models/Winner.cs) and [GamePiece](./Models/GamePiece.cs) class have properties that should belong to it.


#### DRY

The DRY principle (Don't Repeat Yourself) is a programming principle that advocates avoiding code repetition by creating abstractions or reusing existing code. Therefore, there is no repetition of code in the services.


#### KISS 

All the classes in this game are written clearly, so that even without thinking you can understand what task this or that section of code performs



### Design patterns

#### Observer

The [`GamePiece`](./Models/GamePiece.cs) class implements the `INotifyPropertyChanged` interface, which allows objects of this class to notify other objects about changes to their state. When one of the `GamePiece` object's properties changes, the `NotifyPropertyChanged` method is called, which raises the `PropertyChanged` event. This allows other objects to subscribe to and react to changes, providing reactive behavior when the GamePiece's state changes.


#### Command

For example the [`ButtonInteractionVM`](./ViewModels/ButtonInteractionVM.cs) class provides commands for interacting with the user interface. These commands encapsulate operations (`ResetGame`, `SaveGame`, `LoadGame`) and allow them to be called from various user interface elements without directly linking to the source of the operation. This allows you to separate application logic and user interface, providing a more flexible and scalable architecture. The [`GameSquareVM`](./ViewModels/GameSquareVM.cs) class also represents an individual cell of the playing field as a presentation model. It contains commands (`ClickPieceCommand` and `MovePieceCommand`) that are associated with user interaction actions on a cell of the playing field. These commands encapsulate the corresponding operations (`ClickPiece` and `MovePiece`), allowing you to separate the UI interaction logic from the rest of the application. In principle, all classes in [`ViewModels`](./ViewModels/) can represent this pattern.


#### Strategy

The `SwitchTurns`, `FindNeighbours`, and `DisplayRegularMoves` ([GameLogic](./Services/GameLogic.cs) class) methods contain logic that can change depending on game conditions. Each of these methods represents a strategy for a specific action in the game.